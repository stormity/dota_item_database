import sqlite3

class DotaMath:
	def __init__(self, sqlitedbPath):
		self.connection = sqlite3.connect(sqlitedbPath)
		def dict_factory(cursor, row):
			d = {}
			for idx, col in enumerate(cursor.description):
				d[col[0]] = row[idx]
			return d
		self.connection.row_factory = dict_factory
	
	reqdict = {
		"damage" : 0,
		"lifesteal_meele_percent" : 0,
		"lifesteal_ranged_percent" : 0,
		"attack_speed" : 0,
		"attack_range" : 0,
		"spell_range" : 0,
		"magic_resistance_percent" : 0,
		"armor_gain" : 0,
		"armor_reduction" : 0,
		"evasion_percent" : 0,
		"spell_damage_amplification_percent" : 0,
		"spell_lifesteal_percent" : 0,
		"manaloss_reduction_percent" : 0,
		"cooldown_reduction" : 0,
		"movespeed" : 0,
		"movespeed_percent" : 0,
		"physical_damage_block_meele" : 0,
		"physical_damage_block_ranged" : 0,
		"physical_damage_block_chance_percent" : 0,
		"strength" : 0,
		"agility" : 0,
		"intelligence" : 0,
		"manapool" : 0,
		"healthpool" : 0,
		"manaregen" : 0.0,
		"healthregen" : 0.0,
	}
	
	attributes = [
		"damage",
		"lifesteal_meele_percent",
		"lifesteal_ranged_percent",
		"attack_speed",
		"attack_range",
		"spell_range",
		"magic_resistance_percent",
		"armor_gain",
		"armor_reduction",
		"evasion_percent",
		"spell_damage_amplification_percent",
		"spell_lifesteal_percent",
		"manaloss_reduction_percent",
		"cooldown_reduction",
		"movespeed",
		"movespeed_percent",
		"physical_damage_block_meele",
		"physical_damage_block_ranged",
		"physical_damage_block_chance_percent",
		"strength",
		"agility",
		"intelligence",
		"manapool",
		"healthpool",
		"manaregen",
		"healthregen",
	]
		
	def getPureItems(self, attribute):
		if not attribute in DotaMath.attributes :
			return None
		where = " where "
		for attr in DotaMath.attributes :
			if attr == attribute :
				where += attr + " != 0 and "
			else :
				where += attr + " = 0 and "
		where += "unlisted_features = 0"
		cur = self.connection.cursor()
		select = "select * from items" + where
		items = []
		for row in cur.execute(select) :
			items.append(row)
		return items
	
	def getPureAttributeCost(self, attribute):
		items = self.getPureItems(attribute)
		if items is None :
			return None
		cost = 0
		ammount = 0.0
		count = 0
		for item in items :
			cost += item["cost"]
			ammount += item[attribute]
			count += 1
		if count > 0 :
			return cost/ammount
		else :
			return None
	
	def getAttributeCostUsingBasis(self, attribute, basislist) :
		if not attribute in DotaMath.attributes :
			return None
		basis = {}
		for attr in basislist :
			cost = self.getPureAttributeCost(attr)
			if cost is None :
				return None
			basis[attr] = cost
		where = " where "
		for attr in DotaMath.attributes :
			if attr == attribute :
				where += attr + " != 0 and "
			elif not attr in basis :
				where += attr + " = 0 and "
		where += "unlisted_features = 0"
		cur = self.connection.cursor()
		select = "select * from items" + where
		items = []
		for row in cur.execute(select) :
			items.append(row)
		costSum = 0
		attrSum = 0
		for item in items :
			itemCost = item["cost"]
			for attr in basis :
				attributeCost = basis[attr]
				attributeCount = item[attr]
				itemCost -= attributeCost * attributeCount
			costSum += itemCost
			attrSum += item[attribute]
			print(item["name"] + " : cost = " + str(item["cost"]) + "(" + str(itemCost) + ") ; attribute = " + str(item[attribute]))
		return costSum/attrSum






