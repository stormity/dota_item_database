CREATE TABLE `items` (
    `name` TEXT NOT NULL UNIQUE,
    `cost` INTEGER NOT NULL,
    `damage` INTEGER NOT NULL,
    `lifesteal_meele_percent` INTEGER NOT NULL,
    `lifesteal_ranged_percent` INTEGER NOT NULL,
    `attack_speed` INTEGER NOT NULL,
    `attack_range` INTEGER NOT NULL,
    `spell_range` INTEGER NOT NULL,
    `magic_resistance_percent` INTEGER NOT NULL,
    `armor_gain` INTEGER NOT NULL,
    `armor_reduction` INTEGER NOT NULL,
    `evasion_percent` INTEGER NOT NULL,
    `spell_damage_amplification_percent` INTEGER NOT NULL,
    `spell_lifesteal_percent` INTEGER NOT NULL,
    `manaloss_reduction_percent` INTEGER NOT NULL,
    `cooldown_reduction` INTEGER NOT NULL,
    `movespeed` INTEGER NOT NULL,
    `movespeed_percent` INTEGER NOT NULL,
    `physical_damage_block_meele` INTEGER NOT NULL,
    `physical_damage_block_ranged` INTEGER NOT NULL,
    `physical_damage_block_chance_percent` INTEGER NOT NULL,
    `strength` INTEGER NOT NULL,
    `agility` INTEGER NOT NULL,
    `intelligence` INTEGER NOT NULL,
    `manapool` INTEGER NOT NULL,
    `healthpool` INTEGER NOT NULL,
    `manaregen` REAL NOT NULL,
    `healthregen` REAL NOT NULL,
    `consumable` INTEGER NOT NULL,
    `active` INTEGER NOT NULL,
    `cooldown` INTEGER NOT NULL,
    `manacost` INTEGER NOT NULL,
    `range` INTEGER,
    `aoe` INTEGER NOT NULL,
    `charge_buy_count` INTEGER,
    `charge_max_count` INTEGER,
    `unlisted_features` INTEGER,
    `note` TEXT,
    PRIMARY KEY(`name`)
);
